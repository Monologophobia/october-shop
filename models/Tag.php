<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;

use BackendAuth;
use \October\Rain\Database\Model;

class Tag extends Model {

    // The table to use
    public $table = 'mono_shop_product_tags';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'  => 'required|string',
    ];

}
