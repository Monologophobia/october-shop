<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;
use Str;

use BackendAuth;
use \October\Rain\Database\Model;

class Category extends Model {

    // The table to use
    public $table = 'mono_shop_categories';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string',
        'slug' => 'required|string|unique:mono_shop_categories',
    ];

    public $hasMany = [
        'products' => ['Monologophobia\Shop\Models\Product', 'key' => 'category_id', 'order' => 'order asc',]
    ];

    public $attachOne = [
        'image' => ['System\Models\File'],
        'description_image' => ['System\Models\File'],
    ];

    public function getParentIdOptions() {
        $categories = $this->lists('name', 'id');
        $categories[0] = "No Parent";
        // remove itself from the list
        unset($categories[$this->id]);
        return $categories;
    }

    public function beforeValidate() {
        if (!$this->slug) {
            $this->slug = Str::slug($this->name);
        }
        else {
            $this->slug = Str::slug($this->slug);
        }
    }

}
