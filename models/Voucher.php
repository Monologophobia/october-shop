<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;

use BackendAuth;
use \October\Rain\Database\Traits\SoftDeleting;
use \October\Rain\Database\Model;

class Voucher extends Model {

    // The table to use
    public $table = 'mono_shop_vouchers';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'code'   => 'required|string',
        'amount' => 'required'
    ];

    public $hasMany = [
        'orders' => ['Monologophobia\Shop\Models\Order', 'key' => 'voucher_used', 'otherKey' => 'code']
    ];

    public function getTypeOptions() {
        return [1 => 'Percent Off', 2 => 'Flat amount off', 3 => 'Free Shipping'];
    }

}
