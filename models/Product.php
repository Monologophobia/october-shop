<?php namespace Monologophobia\Shop\Models;

use \October\Rain\Database\Model;
use \October\Rain\Database\Traits\SoftDeleting;

use Monologophobia\Shop\Models\Tag;
use Monologophobia\Shop\Models\Seller;

class Product extends Model {

    // Add soft deleting to the model
    use SoftDeleting;
    protected $dates = ['deleted_at'];

    // The table to use
    public $table = 'mono_shop_products';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // generate url slugs for product
    use \October\Rain\Database\Traits\Sluggable;
    protected $slugs = ['slug' => 'name'];

    protected $jsonable = ['custom', 'tags'];
    protected $nullable = ['custom', 'seller_id', 'seller_percentage', 'tags', 'barcode'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'        => 'required|string',
        'category_id' => 'required|integer',
        'price'       => 'required|numeric',
        'weight'      => 'required|integer',
        'cost'        => 'numeric'
    ];

    public $belongsTo = [
        'category' => ['Monologophobia\Shop\Models\Category', 'key' => 'category_id'],
        'seller'   => ['Monologophobia\Shop\Models\Seller', 'key' => 'seller_id']
    ];

    public $hasMany = [
        'reviews' => ['Monologophobia\Shop\Models\ProductReview', 'key' => 'product_id', 'delete' => true],
        'published_reviews' => ['Monologophobia\Shop\Models\ProductReview', 'key' => 'product_id', 'scope' => 'published', 'delete' => true],
        'sales' => ['Monologophobia\Shop\Models\OrderItems', 'key' => 'product_id']
    ];

    public $attachMany = [
        'images' => ['System\Models\File']
    ];

    public $attachOne = [
        'virtual_file'        => ['System\Models\File', 'public' => false],
        'additional_document' => ['System\Models\File'],
    ];

    public function getVirtualFile() {
        return $this->virtual_file->output();
    }

    public function getSellerOptions() {
        $options = [];
        $options[0] = 'No 3rd party seller';
        foreach (Seller::get() as $seller) {
            $options[$seller->id] = $seller->name;
        }
        return $options;
    }

    public function getTagsOptions() {
        $return = [];
        foreach (Tag::get() as $tag) $return[] = $tag->name;
        return $return;
    }

    public function getRating() {
        $rating = 0;
        foreach ($this->published_reviews as $review) {
            $rating += $review->rating;
        }
        $rating = $rating / $this->published_reviews->count();
        return round($rating, 1);
    }

}
