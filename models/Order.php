<?php namespace Monologophobia\Shop\Models;

use DB;
use Crypt;
use Flash;

use BackendAuth;
use \October\Rain\Database\Traits\SoftDeleting;
use \October\Rain\Database\Model;

class Order extends Model {

    // Add soft deleting to the model
    use SoftDeleting;

    // The table to use
    public $table = 'mono_shop_orders';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'shipping_id'    => 'required|integer',
        'shipping_price' => 'required'
    ];

    public $belongsTo = [
        'user' => ['RainLab\User\Models\User', 'key' => 'user_id']
    ];

    public $hasOne = [
        'shipping' => ['Monologophobia\Shop\Models\Shipping', 'key' => 'shipping_id'],
        'voucher'  => ['Monologophobia\Shop\Models\Voucher', 'key' => 'voucher_used', 'otherKey' => 'code']
    ];

    public $hasMany = [
        'items' => ['Monologophobia\Shop\Models\OrderItems', 'key' => 'order_id', 'softDelete' => true, 'delete' => true]
    ];

    use \October\Rain\Database\Traits\Encryptable;
    protected $encryptable = ['bill_to', 'ship_to'];

    public function getShippingOptions() {
        return \Monologophobia\Shop\Models\Shipping::lists('name', 'id');
    }

    public function getSubTotal() {
        $price = 0;
        foreach ($this->items as $item) {
            $price += $item->price * $item->quantity;
        }
        return number_format($price, 2);
    }

    public function getTotal() {
        $price = $this->getSubTotal();
        $price += $this->shipping_price;
        $price -= $this->discount ?? 0;
        return number_format($price, 2);
    }

    public function getStatusAttribute() {
        // 0 not shipping, 1 part shipped, 2 complete
        $count_complete = 0;
        $count_items    = count($this->items);
        foreach ($this->items as $item) {
            if ($item->shipped) $count_complete++;
        }
        if ($count_complete == 0) return 0;
        if ($count_complete < $count_items) return 1;
        if ($count_complete == $count_items) return 2;
    }

}
