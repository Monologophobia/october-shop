<?php namespace Monologophobia\Shop\Models;

use Mail;
use BackendAuth;
use \October\Rain\Database\Model;

use Monologophobia\Shop\Models\Settings;

class Seller extends Model {

    // The table to use
    public $table = 'mono_shop_sellers';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    protected $nullable = ['stripe_id', 'state', 'refresh_token', 'access_token'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'  => 'required|string',
        'email' => 'required|email|unique:mono_shop_sellers',
    ];

    public $belongsTo = [
        'user' => ['Backend\Models\User', 'key' => 'user_id']
    ];

    public $hasMany = [
        'products' => ['Monologophobia\Shop\Models\Product', 'key' => 'seller_id', 'delete' => true]
    ];

    public function afterCreate() {
        $this->sendStripeAuthorisationEmail();
    }

    public function sendStripeAuthorisationEmail() {

        // only ever use the live details. We can generate test credentials from the response
        $live      = boolval(Settings::get('live'));
        $client_id = ($live ? Settings::get('connect_client_id') : Settings::get('test_connect_client_id'));

        // set the seller state for communication with stripe
        $this->state = uniqid();
        $this->save();

        $link  = "https://connect.stripe.com/oauth/authorize";
        $link .= "?response_type=code";
        $link .= "&client_id=" . $client_id;
        $link .= "&scope=read_write";
        $link .= "&state=" . $this->state;
        $link .= "&redirect_uri=" . url('/api/shop/stripe-connect');

        $email = $this->email;
        Mail::send('monologophobia.shop::shop.connect', ['link' => $link], function($message) use ($email) {
            $message->to($email);
        });

    }

}
