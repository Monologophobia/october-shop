<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;
use Mail;

use BackendAuth;
use \October\Rain\Database\Model;

class ProductReview extends Model {

    // The table to use
    public $table = 'mono_shop_products_reviews';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'     => 'required|string',
        'email'    => 'required|string',
        'rating'   => 'required|integer',
        'headline' => 'required|string'
    ];

    public $belongsTo = [
        'product' => ['Monologophobia\Shop\Models\Product', 'key' => 'product_id']
    ];

    public function scopePublished($query) {
        return $query->where('published', true);
    }

    public function beforeCreate() {
        $this->published = false;
    }

    public function afterCreate() {
        $to = \System\Models\MailSetting::get('sender_email');
        $params = ['product_name' => $this->product->name];
        Mail::sendTo($to, 'monologophobia.shop::shop.review', $params);
    }

}
