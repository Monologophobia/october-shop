<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;

use BackendAuth;
use \October\Rain\Database\Model;

class OrderItems extends Model {

    use \October\Rain\Database\Traits\SoftDeleting;

    // The table to use
    public $table = 'mono_shop_orders_items';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    protected $jsonable = ['custom'];
    protected $nullable = ['custom'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'order_id'   => 'required|integer',
        'product_id' => 'required|integer',
        'price'      => 'required',
        'quantity'   => 'required|integer'
    ];

    public $belongsTo = [
        'order'   => ['Monologophobia\Shop\Models\Order', 'key' => 'order_id'],
        'product' => ['Monologophobia\Shop\Models\Product', 'key' => 'product_id']
    ];

    public function scopeForSeller($query) {
        $user     = BackendAuth::getUser();
        $sellers  = [];
        if ($user->sellers) {
            foreach ($user->sellers as $seller) {
                $sellers[] = $seller->id;
            }
            if ($sellers) {
                $products = Product::whereIn('seller_id', $sellers)->lists('id');
                return $query->whereIn('product_id', $products);
            }
        }
        return $query;
    }

}
