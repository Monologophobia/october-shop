<?php namespace Monologophobia\Shop\Models;

use Model;

class Settings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'mono_shop_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}
