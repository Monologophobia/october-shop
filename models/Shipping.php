<?php namespace Monologophobia\Shop\Models;

use DB;
use Flash;

use BackendAuth;
use \October\Rain\Database\Model;

class Shipping extends Model {

    // The table to use
    public $table = 'mono_shop_shipping';

    // Automatically generate created_at and updated_at
    public $timestamps = true;

    protected $nullable = ['free_if_over'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'  => 'required|string',
        'type'  => 'required|integer',
        'price' => 'required'
    ];

    public $hasMany = [
        'orders' => ['Monologophobia\Shop\Models\Order', 'key' => 'shipping_id']
    ];

    public function getTypeOptions() {
        return [
            '1' => 'Flat Rate',
            '2' => 'Per Quantity',
            '3' => 'By Total Weight'
        ];
    }

}
