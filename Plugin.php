<?php namespace Monologophobia\Shop;

use Event as SystemEvent;
use Backend;

use System\Classes\PluginBase;

use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Models\User as UserModel;

/**
 * Shop Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Shop',
            'description' => 'Shop Plugin',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-shopping-cart'
        ];
    }

    // Register plugin permissions
    public function registerPermissions() {
        return [
            'monologophobia.shop.orders'   => ['tab' => 'Shop', 'label' => 'Manage Orders'],
            'monologophobia.shop.products'   => ['tab' => 'Shop', 'label' => 'Change Products'],
            'monologophobia.shop.categories' => ['tab' => 'Shop', 'label' => 'Change Categories'],
            'monologophobia.shop.shipping'   => ['tab' => 'Shop', 'label' => 'Change Shipping'],
            'monologophobia.shop.settings'   => ['tab' => 'Shop', 'label' => 'Alter Shop Settings'],
        ];
    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'shop' => [
                'label'       => 'Shop',
                'url'         => Backend::url('monologophobia/shop/orders'),
                'icon'        => 'icon-shopping-cart',
                'permissions' => ['monologophobia.shop.*'],
                'order'       => 300,

                'sideMenu' => [
                    'orders' => [
                        'label'       => 'Orders',
                        'url'         => Backend::url('monologophobia/shop/orders'),
                        'icon'        => 'icon-briefcase',
                        'permissions' => ['monologophobia.shop.orders']
                    ],
                    'products' => [
                        'label'       => 'Products',
                        'url'         => Backend::url('monologophobia/shop/products'),
                        'icon'        => 'icon-tags',
                        'permissions' => ['monologophobia.shop.products']
                    ],
                    'categories' => [
                        'label'       => 'Categories',
                        'url'         => Backend::url('monologophobia/shop/categories'),
                        'icon'        => 'icon-list',
                        'permissions' => ['monologophobia.shop.categories']
                    ],
                    'shipping' => [
                        'label'       => 'Shipping',
                        'url'         => Backend::url('monologophobia/shop/shipping'),
                        'icon'        => 'icon-ship',
                        'permissions' => ['monologophobia.shop.shipping']
                    ],
                    'vouchers' => [
                        'label'       => 'Vouchers',
                        'url'         => Backend::url('monologophobia/shop/vouchers'),
                        'icon'        => 'icon-bookmark',
                        'permissions' => ['monologophobia.shop.shipping']
                    ],
                    'sellers' => [
                        'label'       => 'Sellers',
                        'url'         => Backend::url('monologophobia/shop/sellers'),
                        'icon'        => 'icon-pie-chart',
                        'permissions' => ['monologophobia.shop']
                    ],
                    'reports' => [
                        'label'       => "Reports",
                        'url'         => Backend::url('monologophobia/shop/reports'),
                        'icon'        => 'icon-area-chart',
                        'permissions' => ['monologophobia.shop.orders']
                    ],
                ]
            ]
        ];
    }

    public function registerSettings() {
        return [
            'stripe' => [
                'label'       => 'Shop Settings',
                'description' => 'Manage general event settings and payment publishable keys and secrets.',
                'category'    => 'Shop',
                'icon'        => 'icon-credit-card',
                'class'       => 'Monologophobia\Shop\Models\Settings',
                'order'       => 500,
                'permissions' => ['monologophobia.shop.settings']
            ]
        ];
    }

    // The emails to send out from this plugin
    public function registerMailTemplates() {
        return [
            'monologophobia.shop::shop.orders'       => 'The order receipt to send to the customer',
            'monologophobia.shop::shop.shipping'     => 'The shipping notification',
            'monologophobia.shop::shop.notification' => 'The order notification to owners',
            'monologophobia.shop::shop.connect'      => 'Seller accounts Stripe connect notification',
            'monologophobia.shop::shop.review'       => 'A product review has been made',
        ];
    }

    // Register front-end components
    public function registerComponents() {
        return [
           '\Monologophobia\Shop\Components\Checkout'    => 'checkout',
           '\Monologophobia\Shop\Components\QuickCart'   => 'quickcart',
           '\Monologophobia\Shop\Components\Category'    => 'category',
           '\Monologophobia\Shop\Components\ProductPage' => 'productpage',
           '\Monologophobia\Shop\Components\History'     => 'history',
           '\Monologophobia\Shop\Components\Search'      => 'search',
        ];
    }

    public function boot() {

        \Backend\Models\User::extend(function($model) {
            $model->hasMany['sellers'] = [
                'Monologophobia\Shop\Models\Seller',
                'key' => 'user_id',
                'delete' => true
            ];
        });

        \RainLab\User\Models\User::extend(function($model) {
            $model->hasMany['shop_orders'] = [
                'Monologophobia\Shop\Models\Order', 'key' => 'user_id', 'delete' => true
            ];
        });

        \RainLab\User\Controllers\Users::extend(function($controller) {

            // Implement the relation controller if it doesn't exist already
            // first if statement doesn't seem to work, despite it being in the behaviour documentation
            //if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
            if (!in_array('Backend.Behaviors.RelationController', $controller->implement)) {
                $controller->implement[] = 'Backend.Behaviors.RelationController';
            }

            // Define property if not already defined
            if (!isset($controller->relationConfig)) $controller->addDynamicProperty('relationConfig');

            // add new relation data
            $myConfigPath = '~/plugins/monologophobia/shop/controllers/users/config_relation.yaml';
            $controller->relationConfig = $controller->mergeConfig(
                $controller->relationConfig,
                $myConfigPath
            );

        });

        // Extend all backend form usage
        SystemEvent::listen('backend.form.extendFields', function($widget) {

            // Only for the User controller and model
            // also prevent tabbed repeater loading all other fields
            if (
                !$widget->getController() instanceof \RainLab\User\Controllers\Users ||
                !$widget->model instanceof \RainLab\User\Models\User
            ) {
                return;
            }

            $widget->addTabFields([
                'shop_orders' => [
                    'type'    => 'partial',
                    'tab'     => 'Shop Orders',
                    'path'    => '~/plugins/monologophobia/shop/controllers/users/_shop_orders.htm'
                ],
            ]);

        });

   }

   public function registerListColumnTypes() {
       return [
           'ordername' => function($value, $column, $record) {

                $bill_to = json_decode($value);

                $name = $bill_to->name;
                if (isset($bill_to->surname)) {
                    $name .= " " . $bill_to->surname;
                }
                
                return $name;

            }
       ];
   }

    public function registerReportWidgets() {
        return [
            'Monologophobia\Shop\ReportWidgets\Overview' => [
                'label'   => 'Shop Overview',
                'context' => 'dashboard',
                'permissions' => [
                    'monologophobia.shop.*',
                ],
            ],
        ];
    }

}
