<?php namespace Monologophobia\Shop\Helpers;

use Request;
use Redirect;
use Session;

use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\Shipping;
use Monologophobia\Shop\Models\Voucher;

class ShopHelpers {

    public static function getCartTotals() {

        $items = Session::get('cart');

        if (!$items || count($items) <= 0) $items = [];

        $weight    = 0;
        $quantity  = 0;
        $return    = new \stdclass;
        $return->products  = [];
        $return->sub_total = 0;

        foreach ($items as $item) {

            $product              = Product::where('id', $item->id)->first();
            $product->cart_id     = $item->cart_id;
            $product->total_price = ShopHelpers::getItemPriceWithQuantity($product, $item->quantity);
            $product->quantity    = $item->quantity;
            $product->custom      = $item->custom;
            array_push($return->products, $product);

            $weight            += $product->weight;
            $quantity          += $item->quantity;
            $return->sub_total += $product->total_price;

        }

        $return->discount = 0;
        $return->discount_amount = 0;
        $return->shipping = $items ? ShopHelpers::getShippingPrice(post('postage'), $return->products, $return->sub_total, $weight, $quantity) : 0;
        $return->total    = $return->shipping + $return->sub_total;

        $voucher = Voucher::where('code', post('voucher'))->first();
        // if there's a voucher in use
        if ($voucher) {
            $return = ShopHelpers::getVoucherDiscount($voucher, $return);
        }

        return $return;

    }

    public static function addItemToCart($id, $quantity = 1, $custom = false) {

        $product = Product::where('id', $id)->first();

        if (!$product || !$product->is_active) {
            return false;
        }

        if ($quantity < 1) $quantity = 1;

        $cart = Session::get('cart');
        if (!$cart) $cart = [];

        // Add to quantity if item already exists in array
        $addition = false;
        foreach ($cart as $key => $item) {
            if ($item->id == $product->id && $item->custom == $custom) {
                $addition = true;
                $cart[$key]->quantity += $quantity;
            }
        }

        // If it didn't, add it to the array
        if (!$addition) {
            $item = new \stdclass;
            $item->id = $product->id;
            $item->cart_id  = uniqid();
            $item->custom   = $custom;
            $item->quantity = $quantity;
            array_push($cart, $item);
            Session::put('cart', $cart);
        }

        return true;

    }

    private static function getItemPriceWithQuantity($product, $quantity) {
        $price = $product->price;
        if ($product->discount_price) $price = $product->discount_price;
        $price = floatval($price);
        $price = $price * $quantity;
        $price = round($price, 2);
        return $price;
    }

    private static function getShippingPrice($id = false, $products, $sub_total, $weight = false, $quantity = false) {

        if ($id) {
            $shipping = Shipping::where('active', 1)->where('id', $id)->firstOrFail();
        }
        else {
            $shipping = Shipping::where('active', 1)->first();
        }

        $price = floatval($shipping->price);
        // Flat Rate
        if ($shipping->type == 1) {
            $price = $price;
        }
        else if ($shipping->type == 2) {
            if (!$quantity) return Redirect::refresh()->with('error', 'Quantity not found');
            $price = $price * $quantity;
            $price = round($price, 2);
        }
        else if ($shipping->type == 3) {
            if (!$weight) return Redirect::refresh()->with('error', 'Weight not found');
            $price = $price * $weight;
            $price = round($price, 2);
        }

        // have we reached the free shipping tier?
        if (is_numeric($shipping->free_if_over) && $sub_total > $shipping->free_if_over) $price = 0;

        // if cart is solely virtual products, shipping is 0
        if (ShopHelpers::isCartSolelyVirtualProducts($products)) $price = 0;

        return $price;

    }

    /**
     * Iterates through supplied products to see if cart is only
     * comprised of virtual products
     * @param array of products
     * @return boolean
     */
    public static function isCartSolelyVirtualProducts($products = []) {
        $solely_virtual_products = false;
        foreach ($products as $product) {
            if ($product->virtual_product) {
                $solely_virtual_products = true;
            }
            else {
                $solely_virtual_products = false;
            }
        }
        return $solely_virtual_products;
    }

    private static function getVoucherDiscount($voucher, $cart) {

        $cart->voucher = $voucher;

        // percentage voucher
        if ($voucher->type == 1) {
            $cart->discount  = intval($voucher->amount);
            $discount_amount = ($cart->sub_total / 100) * $cart->discount;
            $cart->discount_amount = $discount_amount;
            $cart->total     = $cart->sub_total - $discount_amount + $cart->shipping;
        }
        // flat amount
        if ($voucher->type == 2) {
            $cart->discount = $voucher->amount;
            $cart->discount_amount = $cart->discount;
            $cart->total    = $cart->sub_total - $cart->discount + $cart->shipping;
        }
        // free shipping
        if ($voucher->type == 3) {
            $cart->discount = $cart->shipping;
            $cart->discount_amount = $cart->discount;
            $cart->total = $cart->sub_total;
        }

        $cart->total = round($cart->total, 2);
        return $cart;

    }

    /**
     * Gets product page from dropdown
     * Strips out slug
     * @param string url
     * @return string url
     */
    public static function getPageWithoutSlug($property) {
        $page = $property;
        $position = strpos($page, ":");
        if ($position !== false) {
            $page = substr($page, 0, $position);
            $page = rtrim($page, "/");
        }
        return $page;
    }

}
