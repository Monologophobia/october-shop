<?php namespace Monologophobia\Shop\ReportWidgets;

use BackendAuth;
use Backend\Classes\ReportWidgetBase;

use Monologophobia\Shop\Models\Order;
use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\OrderItems;

class Overview extends ReportWidgetBase {

    public function render() {

        $user = BackendAuth::getUser();

        $date = date('Y-m-d H:i:s', strtotime("1 month ago"));
        $needs_shipping     = OrderItems::where('shipped', false);
        $this_months_orders = OrderItems::where('created_at', '>=', $date);

        if ($user->sellers) {
            $sellers = [];
            foreach ($user->sellers as $seller) {
                $sellers[] = $seller->id;
            }
            if ($sellers) {

                // get all products belonging to seller
                $seller_products = Product::whereIn('seller_id', $sellers)->lists('id');

                // only return order items that are the seller's products
                $needs_shipping->whereIn('product_id', $seller_products);

                // get order items in the last month that are only the seller's products
                $this_months_orders->whereIn('product_id', $seller_products);

            }
        }

        $needs_shipping     = $needs_shipping->get();
        $this_months_orders = $this_months_orders->get();

        $this->vars['needs_shipping']     = $needs_shipping;
        $this->vars['this_months_orders'] = $this_months_orders;
        $this->vars['orders']             = Order::whereIn('id', $this_months_orders->lists('id'))->get();

        return $this->makePartial('widget');

    }

}