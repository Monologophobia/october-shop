<?php namespace Monologophobia\Shop\Controllers;

use Flash;
use Redirect;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Seller;

class Sellers extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.shop.*'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'sellers');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $seller = Seller::findOrFail($id);
                $seller->delete();
            }
            Flash::success('Deleted Successfully');

        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

    public function update_onResendStripeConnectEmail($id) {
        try {
            $seller = Seller::findOrFail(intval($id));
            $seller->sendStripeAuthorisationEmail();
            Flash::success('Stripe Connect email resent');
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
        }
    }

}
