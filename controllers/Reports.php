<?php namespace Monologophobia\Shop\Controllers;

use DateTime;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Order;
use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\Category;
use Monologophobia\Shop\Models\OrderItems;


class Reports extends \Backend\Classes\Controller {

    public $requiredPermissions = ['monologophobia.shop.reports.*'];

    public $pageTitle = 'Shop Reports';

    public $from;
    public $to;

    public function __construct() {

        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'reports');

        $this->from = new DateTime("3 months ago");
        $this->to   = new DateTime();

    }

    public function index() {
        $this->vars['from'] = $this->from->format('Y-m-d');
        $this->vars['to']   = $this->to->format('Y-m-d');
        $this->getOverview();
        $this->getMonthlySales();
    }

    public function onGetData() {
        $this->from = new DateTime(post('from'));
        $this->to   = new DateTime(post('to'));
        $this->vars['from'] = $this->from->format('Y-m-d');
        $this->vars['to']   = $this->to->format('Y-m-d');
        $this->getOverview();
        $this->getMonthlySales();
        return [
            "#overview" => $this->makePartial('overview'),
            "#monthly" => $this->makePartial('monthly'),
            "#best-worst" => $this->makePartial('bestworst'),
        ];
    }

    private function getOverview() {

        $categories = Category::get();
        $orders     = Order::whereBetween('created_at', [$this->from->format('Y-m-d 00:00:00'), $this->to->format('Y-m-d 23:59:59')])->get();

        $total    = 0;
        $shipping = 0;
        $items    = 0;
        $shipped  = 0;
        $products = [];

        foreach ($orders as $order) {

            $total    += $order->getTotal();
            $shipping += $order->shipping_price;

            foreach ($order->items as $item) {

                $product = ["id" => $item->product_id, "price" => $item->price, "quantity" => $item->quantity];
                $products[] = (object) $product;

                if ($item->shipped) $shipped++;
                $items += $item->quantity;

            }

        }

        foreach ($categories as $category) {

            $category->sold_items  = 0;
            $category->sold_totals = 0;

            foreach ($category->products as $product) {

                $product->sold_items  = 0;
                $product->sold_totals = 0;

                foreach ($products as $key => $p) {

                    if ($p->id == $product->id) {

                        $price = $p->price * $p->quantity;

                        $product->sold_items  += $p->quantity;
                        $product->sold_totals += $price;

                        $category->sold_items  += $p->quantity;
                        $category->sold_totals += $price;

                        unset($products[$key]);

                    }

                }
            }

        }

        $this->vars["total"]      = $total;
        $this->vars["shipping"]   = $shipping;
        $this->vars["items"]      = $items;
        $this->vars["shipped"]    = $shipped;
        $this->vars["categories"] = $categories;

    }

    private function getMonthlySales() {
        $orders = Order::where('created_at', '>=', $this->to->modify("-1 year")->format('Y-m-d 00:00:00'))->get();
        $orders = $orders->groupBy(function($model) {
            $date = new DateTime($model->created_at);
            return $date->format("Y-m");
        });
        $this->vars['monthly_sales'] = $orders;
    }

}
