<?php namespace Monologophobia\Shop\Controllers;

use Flash;
use Mail;
use Redirect;
use Response;
use Storage;

use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;

use RainLab\User\Models\User;
use Monologophobia\Shop\Models\Order;
use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\Shipping;
use Monologophobia\Shop\Models\OrderItems;

class Orders extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['monologophobia.shop.orders'];

    public $bodyClass = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'orders');
    }

    public function listInjectRowClass($record, $definition = null) {
        if ($record->status == 1) return 'frozen';
        if ($record->status == 2) return 'positive';
    }

    public function listExtendQuery($query) {
        $this->getOrdersOnlyApplicableToSeller($query);
    }

    public function formExtendQuery($query) {
        $this->getOrdersOnlyApplicableToSeller($query);
    }

    private function getOrdersOnlyApplicableToSeller($query) {
        $user = BackendAuth::getUser();
        if ($user->sellers && count($user->sellers) > 0) {
            $order_items = OrderItems::forSeller()->lists('order_id');
            $query->whereIn('id', $order_items);
        }
    }

    public function onUpdateOrder($id = null) {

        $order = $this->buildOrder($id);
        $order->save(null, post('_session_key', false));

        Flash::success('Order updated');
        return Redirect::to('/backend/monologophobia/shop/orders');

    }

    public function onCustomCreate() {

        $order = $this->buildOrder(false);
        $order->save();

        Flash::success('Order created');
        return Redirect::to('/backend/monologophobia/shop/orders/update/' . $order->id);

    }

    private function buildOrder($id = null) {

        $order = $id ? $this->formFindModelObject($id) : new Order;

        // add shipping options
        $shipping = Shipping::findOrFail(intval(post('Order[shipping]')));
        $order->shipping_id    = $shipping->id;
        $order->shipping_price = $shipping->price;

        // Add billing and shipping data
        $billing  = $this->sanitiseBillingShippingData(post('Order[bill_to]'));
        $shipping = $this->sanitiseBillingShippingData(post('Order[ship_to]'));
        $order->bill_to = json_encode($billing);
        $order->ship_to = json_encode($shipping);

        // Add user if necessary
        $order->user_id = intval(post('Order[user]')) ? User::findOrFail(intval(post('Order[user]')))->id : false;

        $order->shipped = $order->shipped ? $order->shipped : 0;

        return $order;

    }

    /**
     * Sanitise billing or shipping data as it may come directly from POST
     * @param Array
     * @return Array sanitised
     */
    private function sanitiseBillingShippingData(array $data) {
        return [
            "name"     => filter_var($data["name"], FILTER_SANITIZE_STRING),
            "surname"  => filter_var($data["surname"], FILTER_SANITIZE_STRING),
            "email"    => filter_var($data["email"], FILTER_SANITIZE_EMAIL),
            "address1" => filter_var($data["address1"], FILTER_SANITIZE_STRING),
            "address2" => filter_var($data["address2"], FILTER_SANITIZE_STRING),
            "town"     => filter_var($data["town"], FILTER_SANITIZE_STRING),
            "country"  => filter_var($data["country"], FILTER_SANITIZE_STRING),
            "postcode" => filter_var($data["postcode"], FILTER_SANITIZE_STRING),
        ];
    }

    private function insertOrderItem($order_id, $product) {
        $order_item = new OrderItems;
        $order_item->order_id   = $order_id;
        $order_item->product_id = $product->id;
        $order_item->quantity   = $product->quantity;
        $order_item->custom     = $product->custom;
        $order_item->price      = floatval($product->price);
        $order_items->shipped   = false;
        if ($product->discount_price) {
            $order_item->price  = floatval($product->discount_price);
        }
        $order_item->save();
    }

    public function onComplete($recordId) {
        $order = $this->formFindModelObject($recordId);
        $this->shipItems($order->items->lists('id'), $recordId);
        return Redirect::to('/backend/monologophobia/shop/orders');
    }

    public function onCancel($recordId = null) {
        $order = $this->formFindModelObject($recordId);
        $order->delete();
        Flash::success('Order Cancelled!');
        return Redirect::to('/backend/monologophobia/shop/orders');
    }

    public function onShip($recordId) {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            $this->shipItems($checkedIds, $recordId);
        }
        return Redirect::to("/backend/monologophobia/shop/orders/update/{$recordId}");
    }

    private function shipItems($array_of_item_ids, $recordId) {

        $items = [];
        foreach ($array_of_item_ids as $id) {
            $item = OrderItems::with('product')->where('order_id', $recordId)->findOrFail($id);
            $item->shipped = true;
            $item->save();
            $items[] = $item;
        }

        try {

            $order   = $this->formFindModelObject($recordId);
            $billing = json_decode($order->bill_to);
    
            $params = [
                'date'     => date('Y-m-d'),
                'shipping' => json_decode($order->ship_to),
                'items'    => $items,
            ];
    
            Mail::sendTo($billing->email, 'monologophobia.shop::shop.shipping', $params);

        }
        catch (\Exception $e) { }

        Flash::success('Marked as shipped and customer notified');

    }

    public function clickAndDropExport($recordId) {

        $order = $this->formFindModelObject($recordId);

        if (!$order) {
            Flash::error("Order not found");
            return;
        }

        $array_headers = [
            "First Name",
            "Last Name",
            "Address Line 1",
            "Address Line 2",
            "Address Line 3",
            "City",
            "County",
            "Postcode",
            "Email",
            "Order Reference",
            "Date",
            "Weight",
            "Sub-total",
            "Shipping Cost",
            "Total",
        ];

        $weight = 0;
        foreach ($order->items as $item) $weight += $item->product->weight;
        // convert to kg
        $weight = round($weight / 1000, 1);

        $shipping = json_decode($order->ship_to);

        $array_data = [
            $shipping->name,
            $shipping->surname,
            $shipping->address1,
            $shipping->address2,
            "",
            $shipping->town,
            $shipping->country,
            $shipping->postcode,
            $shipping->email,
            $order->id,
            $order->created_at,
            $weight,
            number_format($order->items()->sum('price'), 2),
            number_format($order->shipping_price, 2),
            $order->getTotal(),
        ];

        $filename = "shipping-{$order->id}.csv";
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => "attachment; filename={$filename}",
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        // stream csv output
        $callback = function() use ($array_headers, $array_data) {
            $fh = fopen('php://output', 'w');
            fputcsv($fh, $array_headers);
            fputcsv($fh, $array_data);
            fclose($fh);
        };

        return \Response::stream($callback, 200, $headers);

    }

}
