<?php namespace Monologophobia\Shop\Controllers;

use Flash;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Shipping as ShippingModel;

class Shipping extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.shop.shipping'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'shipping');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $id) {
                $level = ShippingModel::findOrFail($id);
                $level->delete();
            }

            Flash::success('Deleted Successfully');

        }

        else {
            Flash::error('Couldn\'t find the IDs');
        }

        return $this->listRefresh();

    }
    
}
