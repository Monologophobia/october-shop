<?php namespace Monologophobia\Shop\Controllers;

use Flash;
use Carbon\Carbon;
use DateTime;
use BackendMenu;
use ValidationException;

use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\OrderItems;

class Products extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['monologophobia.shop.products'];

    public $bodyClass  = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'products');
    }

    public function index_onDelete() {

        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $id) {
                $level = Product::findOrFail($id);
                $level->delete();
            }

            Flash::success('Deleted Successfully');

        }

        else {
            Flash::error('Couldn\'t find the IDs');
        }

        return $this->listRefresh();

    }

    public function onSalesByPeriod($recordId) {

        $product  = $this->formFindModelObject($recordId);
        $from     = date('Y-m-d 00:00:00', strtotime(post('from')));
        $from_dt  = new DateTime($from);
        $to       = date('Y-m-d 23:59:59', strtotime(post('to')));
        $to_dt    = new DateTime($to);
        $interval = $from_dt->diff($to_dt);

        $by_frequency = intval(post('by_frequency'));

        // check requested data length is not excessive
        if ($by_frequency === 0 || $by_frequency === 1) {
            // if daily reports, limit to 3 months. weekly, 3 months
            $excessive = $by_frequency === 0 ? 3 : 12;
            $months_ahead = $interval->m + ($interval->y * 12);
            if ($months_ahead > $excessive) {
                throw new ValidationException(['_order_history' => 'Requested data exceeds allowed date range']);
            }
        }

        $sales = OrderItems::select('quantity', 'created_at')->where('product_id', $product->id)->whereBetween('created_at', [$from, $to])->get();
        $sales = $this->groupSalesByFrequency($sales, $by_frequency);
        $sales = $this->generateDatesAndQuantities($sales, $from_dt, $to_dt, $by_frequency);

        return ['results' => $sales];

    }

    private function groupSalesByFrequency($sales, int $frequency) {
        $format = 'Y-m-d';
        if ($frequency == 1) $format = 'Y-W';
        if ($frequency == 2) $format = 'Y-m';
        return $sales->groupBy(function($model) use ($format) {
            return Carbon::parse($model->created_at)->format($format);
        });
    }

    private function generateDatesAndQuantities($sales, DateTime $from, DateTime $to, int $frequency) {

        $modify = "+1 day";
        $sales_format = "Y-m-d";
        if ($frequency == 1) {
            $modify = "+1 week";
            $sales_format = "Y-W";
        }
        if ($frequency == 2) {
            $modify = "+1 month";
            $sales_format = "Y-m";
        }

        $return = [];
        // create array of days and add quantities
        while ($from <= $to) {

            $key = $from->format('Y-m-d');
            $return[$key] = 0;
            if (isset($sales[$from->format($sales_format)])) {
                foreach ($sales[$from->format($sales_format)] as $sale) {
                    $return[$key] += $sale->quantity;
                }
            }

            $from->modify($modify);

        }

        return $return;

    }

}
