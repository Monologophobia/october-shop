<?php namespace Monologophobia\Shop\Controllers;

use Flash;
use Redirect;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Tag;

class Tags extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.shop.*'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'products');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $tag = Tag::findOrFail($id);
                $tag->delete();
            }
            Flash::success('Deleted Successfully');

        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

}
