<?php namespace Monologophobia\Shop\Controllers;

use Flash;
use Mail;
use Redirect;

use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Shop\Models\Voucher;

class Vouchers extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.shop.*'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Shop', 'shop', 'vouchers');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $level = Voucher::findOrFail($id);
                $level->delete();
            }
            Flash::success('Deleted Successfully');

        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

}
