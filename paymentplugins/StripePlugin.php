<?php namespace Monologophobia\Shop\PaymentPlugins;

use Response;

use Monologophobia\Shop\Models\Settings;

use Stripe;

class StripePlugin {

    public function getKey() {
        $live = Settings::get('live');
        $key  = boolval($live) ? "app_publishable" : "test_publishable";
        return Settings::get($key);
    }

    public function process($user, $cart, $data) {
        $return = $this->paymentIntent($user, $cart->total, $data->description, $user->email, post('payment_method_id'), post('payment_intent_id'));
        if ($return->requires_action || $return->error) {
            return Response::json($return);
        }
        return $return;
    }

    /**
     * Create or confirm a Payment Intent
     * @param User
     * @param Array of post'd data
     * @param Numeric amount (if float, will be automatically converted to pence)
     * @param String description for receipt
     * @param String email for receipt
     * @param String optional payment_method_id
     * @param String optional payment_intent_id
     * @return Object {requires_action: boolean, payment_intent_client_secret: String}
     * @throws Exception
     */
    private function paymentIntent($user, $amount, $description, $email, $method = false, $intent = false) {

        if (!$method && !$intent) throw new Exception('Method or Intent must be specified');

        $live = boolval(Settings::get('live'));
        $key  = ($live ? Settings::get('app_secret') : Settings::get('test_secret'));
        \Stripe\Stripe::setApiKey($key);

        $payment_intent = false;
        $return = ['requires_action' => false, 'payment_intent_client_secret' => false, 'error' => false];

        try {

            // Do intent retrieval first as that is more important on the second round trip
            if ($intent) {
                $payment_intent = \Stripe\PaymentIntent::retrieve($intent);
                $payment_intent->confirm();
            }
            else if ($method) {
                if (is_float($amount)) $amount = $amount * 100;
                $payment_intent = \Stripe\PaymentIntent::create([
                    'amount'      => $amount,
                    'currency'    => 'GBP',
                    'confirm'     => true,
                    'description' => post('description'),
                    'receipt_email'  => $email,
                    'payment_method' => $method,
                    'confirmation_method' => 'manual',
                ]);
            }

            if ($payment_intent->status == 'requires_action' && $payment_intent->next_action->type == 'use_stripe_sdk') {
                $return['requires_action'] = true;
                $return['payment_intent_client_secret'] = $payment_intent->client_secret;
            }
            else if ($payment_intent->status == 'succeeded') {
                $return['payment_id'] = $payment_intent->id;
            }

        }
        catch (\Stripe\Error\Base $e) {
            $return['error']   = true;
            $return['message'] = $e->getMessage();
        }

        return (object) $return;

    }

    public function getChargeFromPaymentIntent($payment_intent_id) {
        try {
            $live = boolval(Settings::get('live'));
            $key  = ($live ? Settings::get('app_secret') : Settings::get('test_secret'));
            \Stripe\Stripe::setApiKey($key);
            $payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            if ($payment_intent->charges) {
                if (count($payment_intent->charges->data) > 0) {
                    return $payment_intent->charges->data[0];
                }
            }
        }
        catch (\Throwable $e) {
            return false;
        }
    }


}