<?php namespace Monologophobia\Shop\PaymentPlugins;

use Monologophobia\Shop\Models\Settings;

class TotalProcessingPlugin {

    public function getKey() {
        $live = Settings::get('live');
        $key  = boolval($live) ? "bearer_token" : "test_bearer_token";
        return Settings::get($key);
    }

    public function process($user, $cart, $data) {
        $checkout = $this->prepareCheckout($cart->total);
    }

    public function prepareCheckout($amount) {

        $live = boolval(Settings::get('live'));
        $bearer_token = ($live ? Settings::get('bearer_token') : Settings::get('test_bearer_token'));
        $entity_id    = ($live ? Settings::get('entity_id') : Settings::get('test_entity_id'));

        if (is_float($amount)) $amount = number_format($amount, 2);

        if ($live) {
            $url = "https://oppwa.com/v1/checkouts";
        }
        else {
            $url = "https://test.oppwa.com/v1/checkouts";
        }

        $data = "entityId=$entity_id&amount=$amount&currency=GBP&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:Bearer $bearer_token"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $live);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData = json_decode($responseData);

        if ($live) {
            $responseData->script_src = "https://oppwa.com/v1/paymentWidgets.js?checkoutId=";
        }
        else {
            $responseData->script_src = "https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=";
        }
        $responseData->script_src .= $responseData->id;

        $responseData->submit_url = url()->current() . "?complete=true";

        return $responseData;

    }

    function getPaymentStatus(String $checkout_path) {

        $live = boolval(Settings::get('live'));
        $bearer_token = ($live ? Settings::get('bearer_token') : Settings::get('test_bearer_token'));
        $entity_id    = ($live ? Settings::get('entity_id') : Settings::get('test_entity_id'));

        if ($live) {
            $url = "https://oppwa.com";
        }
        else {
            $url = "https://test.oppwa.com";
        }

        $url .= $checkout_path;
        $url .= "?entityId=$entity_id";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization:Bearer $bearer_token"));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $live);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData = json_decode($responseData);

        return $responseData;

    }

}