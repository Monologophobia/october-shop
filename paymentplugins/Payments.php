<?php namespace Monologophobia\Shop\PaymentPlugins;

use Response;

use Monologophobia\Shop\Models\Settings;
use Monologophobia\Shop\PaymentPlugins\StripePlugin;
use Monologophobia\Shop\PaymentPlugins\TotalProcessingPlugin;

class Payments {

    public $processor;
    private $plugin;

    public function __construct() {
        $this->processor = Settings::get('processor', 'stripe');
        $this->plugin    = $this->processor == "stripe" ? new StripePlugin : new TotalProcessingPlugin;
    }

    public function getKey() {
        return $this->plugin->getKey();
    }

    public function process($user, $cart, $data) {
        return $this->plugin->process($user, $cart, $data);
    }

    public function getChargeFromPaymentIntent($payment_intent_id) {
        return $this->plugin->getChargeFromPaymentIntent($payment_intent_id);
    }

}