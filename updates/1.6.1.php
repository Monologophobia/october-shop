<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointSixPointOne extends Migration {

    public function up() {

        Schema::table('mono_shop_products_reviews', function($table) {
            $table->string('location')->nullable();
            $table->string('headline')->nullable();
        });

    }

    public function down() {
        Schema::table('mono_shop_products_reviews', function($table) {
            $table->dropColumn('location');
            $table->dropColumn('headline');
        });
    }

}
