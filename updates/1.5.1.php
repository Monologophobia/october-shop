<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointFivePointOne extends Migration {

    public function up() {

        Schema::table('mono_shop_products', function($table) {
            $table->string('tagline')->nullable();
        });

    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropForeign('tagline');
        });
    }

}
