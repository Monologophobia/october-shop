<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointSevenPointZero extends Migration {

    public function up() {

        Schema::table('mono_shop_orders', function($table) {
            $table->dropColumn('shipped');
        });

        Schema::table('mono_shop_orders_items', function($table) {
            $table->boolean('shipped')->default(false);
        });

    }

    public function down() {

        Schema::table('mono_shop_orders', function($table) {
            $table->boolean('shipped')->default(false);;
        });

        Schema::table('mono_shop_orders_items', function($table) {
            $table->dropColumn('shipped');
        });

    }

}
