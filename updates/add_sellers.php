<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddSellers extends Migration {

    public function up() {

        Schema::create('mono_shop_sellers', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->boolean('notify')->default(true);
            $table->string('stripe_id')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
        });

        Schema::table('mono_shop_products', function($table) {
            $table->integer('seller_id')->nullable()->unsigned();
            $table->foreign('seller_id')->references('id')->on('mono_shop_sellers')->onDelete('cascade');
        });

    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropForeign('seller_id');
            $table->dropColumn('seller_id');
        });
        Schema::dropIfExists('mono_shop_sellers');
    }

}
