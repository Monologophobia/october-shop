<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointEightPointZero extends Migration {

    public function up() {
        Schema::table('mono_shop_sellers', function($table) {
            $table->integer('user_id')->nullable()->unsigned();
        });
    }

    public function down() {
        Schema::table('mono_shop_sellers', function($table) {
            $table->dropColumn('user_id');
        });
    }

}
