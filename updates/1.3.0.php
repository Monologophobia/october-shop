<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointThreePointZero extends Migration {

    public function up() {

        Schema::table('mono_shop_shipping', function($table) {
            $table->float('free_if_over')->nullable();
        });

    }

    public function down() {

        Schema::table('mono_shop_shipping', function($table) {
            $table->dropColumn('free_if_over');
        });

    }

}
