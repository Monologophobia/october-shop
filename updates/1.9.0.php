<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointNinePointZero extends Migration {

    public function up() {
        Schema::table('mono_shop_orders_items', function($table) {
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::table('mono_shop_orders_items', function($table) {
            $table->dropColumn('deleted_at');
        });
    }

}
