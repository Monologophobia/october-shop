<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class StripeConnect extends Migration {

    public function up() {
        Schema::table('mono_shop_products', function($table) {
            $table->string('notification_email')->nullable();
            $table->string('seller_stripe_id')->nullable();
            $table->float('seller_percentage')->nullable();
        });
    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropColumn('notification_email');
            $table->dropColumn('seller_stripe_id');
            $table->dropColumn('seller_percentage');
        });
    }

}
