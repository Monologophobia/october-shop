<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddStripeConnectCredentials extends Migration {

    public function up() {
        Schema::table('mono_shop_sellers', function($table) {
            $table->string('refresh_token')->nullable();
            $table->string('access_token')->nullable();
        });
    }

    public function down() {
        Schema::table('mono_shop_sellers', function($table) {
            $table->dropColumn('access_token');
            $table->dropColumn('refresh_token');
        });
    }

}
