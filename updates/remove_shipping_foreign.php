<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class RemoveShippingForeign extends Migration {

    public function up() {

        Schema::table('mono_shop_orders', function($table) {
            $table->dropForeign(['shipping_id']);
            $table->dropColumn('shipping_id');
        });

        Schema::table('mono_shop_orders', function($table) {
            $table->integer('shipping_id')->default(0);
        });

    }

    public function down() {

        Schema::table('mono_shop_orders', function($table) {
            $table->dropColumn('shipping_id');
            $table->integer('shipping_id')->index()->unsigned();
            $table->foreign('shipping_id')->references('id')->on('mono_shop_shipping');
        });

    }

}
