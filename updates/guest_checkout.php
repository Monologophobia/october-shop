<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class GuestCheckout extends Migration {

    public function up() {

        Schema::table('mono_shop_orders', function($table) {
            $table->dropForeign(['user_id']);
            $table->dropIndex(['user_id']);
            $table->integer('user_id')->nullable()->unsigned()->change();
        });

    }

    public function down() {

        Schema::table('mono_shop_orders', function($table) {
            $table->integer('user_id')->index()->unsigned()->change();
        });

    }

}

