<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointSixPointZero extends Migration {

    public function up() {

        Schema::create('mono_shop_products_reviews', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->nullable()->unsigned();
            $table->foreign('product_id')->references('id')->on('mono_shop_products')->onDelete('cascade');
            $table->string('name');
            $table->string('email');
            $table->integer('rating')->default(5);
            $table->text('comments')->nullable();
            $table->boolean('published')->default(false);
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_shop_product_reviews');
    }

}
