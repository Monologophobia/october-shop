<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointFivePointZero extends Migration {

    public function up() {

        Schema::create('mono_shop_product_tags', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('mono_shop_products', function($table) {
            $table->text('tags')->nullable();
        });

    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropForeign('tags');
        });
        Schema::dropIfExists('mono_shop_product_tags');
    }

}
