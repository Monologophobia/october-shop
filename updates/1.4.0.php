<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointFourPointZero extends Migration {

    public function up() {

        Schema::table('mono_shop_products', function($table) {
            $table->integer('stock_quantity')->default(0);
        });

    }

    public function down() {

        Schema::table('mono_shop_products', function($table) {
            $table->dropColumn('stock_quantity');
        });

    }

}
