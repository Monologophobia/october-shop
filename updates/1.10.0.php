<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointTenPointZero extends Migration {

    public function up() {
        Schema::table('mono_shop_products', function($table) {
            $table->double('cost', 8, 2)->default(0);
            $table->string('barcode')->nullable()->index();
        });
    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropColumn('cost');
            $table->dropColumn('barcode');
        });
    }

}
