<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class FlatRateVouchers extends Migration {

    public function up() {
        Schema::table('mono_shop_vouchers', function($table) {
            $table->integer('type')->default(1);
        });
    }

    public function down() {
        Schema::table('mono_shop_vouchers', function($table) {
            $table->dropColumn('type');
        });
    }

}
