<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class VirtualProducts extends Migration {

    public function up() {
        Schema::table('mono_shop_products', function($table) {
            $table->boolean('virtual_product')->default(false);
        });
    }

    public function down() {
        Schema::table('mono_shop_products', function($table) {
            $table->dropColumn('virtual_product');
        });
    }

}
