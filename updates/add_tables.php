<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddTables extends Migration {

    public function up() {

        Schema::table('users', function($table) {
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('town')->nullable();
            $table->string('country')->nullable();
            if (!Schema::hasColumn('users', 'postcode')) {
                $table->string('postcode')->nullable();
            }
            $table->string('telephone')->nullable();
        });

        Schema::create('mono_shop_categories', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('mono_shop_products', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->float('price')->nullable();
            $table->float('discount_price')->nullable();
            $table->integer('weight')->default(0);
            $table->json('custom')->nullable();
            $table->integer('category_id')->index()->unsigned();
            $table->foreign('category_id')->references('id')->on('mono_shop_categories');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mono_shop_shipping', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('type')->nullable();
            $table->float('price')->nullable();
            $table->integer('active')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mono_shop_vouchers', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code');
            $table->float('amount')->default(0);
            $table->timestamps();
        });

        Schema::create('mono_shop_orders', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('shipping_id')->index()->unsigned();
            $table->foreign('shipping_id')->references('id')->on('mono_shop_shipping');
            $table->float('shipping_price')->nullable();
            $table->integer('shipped')->default(0);
            $table->string('voucher_used')->nullable();
            $table->float('discount')->nullable();
            $table->text('bill_to')->nullable();
            $table->text('ship_to')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mono_shop_orders_items', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->index()->unsigned();
            $table->foreign('order_id')->references('id')->on('mono_shop_orders')->onDelete('cascade');
            $table->integer('product_id')->index()->unsigned();
            $table->foreign('product_id')->references('id')->on('mono_shop_products');
            $table->integer('quantity')->default(1);
            $table->float('price')->nullable();
            $table->json('custom')->nullable();
            $table->timestamps();
        });

    }

    public function down() {

        Schema::dropIfExists('mono_shop_orders_items');
        Schema::dropIfExists('mono_shop_orders');
        Schema::dropIfExists('mono_shop_vouchers');
        Schema::dropIfExists('mono_shop_shipping');
        Schema::dropIfExists('mono_shop_products');
        Schema::dropIfExists('mono_shop_categories');

        Schema::table('users', function($table) {
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('town');
            $table->dropColumn('telephone');
            $table->dropColumn('postcode');
            $table->dropColumn('country');
        });

    }

}
