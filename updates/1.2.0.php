<?php namespace Monologophobia\Shop\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class OnePointTwoPointZero extends Migration {

    public function up() {

        Schema::table('mono_shop_products', function($table) {
            $table->integer('order')->default(0);
        });

    }

    public function down() {

        Schema::table('mono_shop_products', function($table) {
            $table->dropColumn('order');
        });

    }

}
