$(document).ready(function() {

    var input_quantity = document.querySelectorAll('#totals form.quantity input[name="quantity"]');
    for (var i = 0; i < input_quantity.length; i++) {
        input_quantity[i].onchange = function() {
            $(this.form).request('onUpdateExactQuantity');
        }
    }

    var shipping = document.getElementsByName('_shipping_same_as_billing');
    var shipping_details = document.getElementById('shipping-details');
    for (var i = 0; i < shipping.length; i++) {
        shipping[i].onclick = function() {
            if (this.value == 1) {
                shipping_details.style.display = "none";
            }
            else {
                shipping_details.style.display = "block";
            }
        }
    }

    $('#checkout .same-shipping').click(function(e) {
        e.preventDefault();
        $('[name="shipping[name]"]').val($('[name="billing[name]"]').val()).blur();
        $('[name="shipping[surname]"]').val($('[name="billing[surname]"]').val()).blur();
        $('[name="shipping[email]"]').val($('[name="billing[email]"]').val()).blur();
        $('[name="shipping[telephone]"]').val($('[name="billing[telephone]"]').val()).blur();
        $('[name="shipping[address1]"]').val($('[name="billing[address1]"]').val()).blur();
        $('[name="shipping[address2]"]').val($('[name="billing[address2]"]').val()).blur();
        $('[name="shipping[town]"]').val($('[name="billing[town]"]').val()).blur();
        $('[name="shipping[country]"]').val($('[name="billing[country]"]').val()).blur();
        $('[name="shipping[postcode]"]').val($('[name="billing[postcode]"]').val()).blur();
        $('#checkout select[name="shipping[country]"]').trigger('change');
    });

    $('#checkout [name="postage"]').change(function() {
        $(this).request('onUpdateShipping', {
            method: 'POST',
            success: function(data) {
                updatePrices(data);
            }
        });
    });

    $('#checkout #check-voucher').click(function(e) {
        e.preventDefault();
        $(this).request('onUpdateShipping', {
            method: 'POST',
            success: function(data) {
                updatePrices(data);
            }
        });
    });

    function updatePrices(data) {
        // onUpdateShipping calls getCartTotals which does both shipping and voucher
        var result = JSON.parse(data.result);
        $('#checkout #totals .discount').html('');
        $('#checkout .voucher .message').remove();
        $('#checkout #totals .shipping').html(result.shipping.toFixed(2));
        $('#checkout #totals span.total').html(result.total.toFixed(2));
        var discount = result.discount.toFixed(2);
        if (result.voucher) {
            if (result.voucher.type == 1) {
                discount = discount + '%';
            }
            if (result.voucher.type == 2) {
                discount = '£' + discount;
            }
            if (result.voucher.type == 3) {
                discount = 'Free Shipping: £' + discount;
            }
        }
        if (result.discount > 0) {
            $('#checkout #totals .discount').html('<div class="discount">Discount: ' + discount + '</div>');
            $('#checkout .voucher').append('<div class="message">Voucher Applied</div>');
        }
        $('#checkout #totals input[name="total"]').val(result.total * 100);
    }

    var guest_checkout = document.getElementsByName('guest_checkout');
    var password       = document.getElementById('password');
    if (guest_checkout && guest_checkout.length > 0) {
        for (var i = 0; i < guest_checkout.length; i++) {
            guest_checkout[i].addEventListener('change', function() {
                if (this.value == 1) {
                    password.style.display = "none";
                    document.getElementsByName('billing[password]')[0].required = false;
                    document.getElementsByName('billing[password_confirmation]')[0].required = false;
                    password.required = false;
                }
                else {
                    password.style.display = "block";
                    document.getElementsByName('billing[password]')[0].required = true;
                    document.getElementsByName('billing[password_confirmation]')[0].required = true;
                }
            });
        }
    }

    var form = document.getElementById('checkout');
    form.addEventListener('submit', function(event) {

        event.preventDefault();

        var name = document.querySelector('input[name="billing[name]"]').value;
        name    += " ";
        name    += document.querySelector('input[name="billing[surname]"]').value;
        var billing_details = {
            name: name,
            email: document.querySelector('input[name="billing[email]"]').value,
            address: {
                'line1': document.querySelector('input[name="billing[address1]"]').value,
                'line2': document.querySelector('input[name="billing[address2]"]').value,
                'city': document.querySelector('input[name="billing[town]"]').value,
                'postal_code': document.querySelector('input[name="billing[postcode]"]').value,
            }
        };

        handlePayment(form, billing_details);

    });

});
