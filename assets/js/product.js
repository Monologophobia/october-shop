$('#product .images a').magnificPopup({
    type: 'image',
    gallery: {
        enabled:true
    }
});

$('#product .images').slick({
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    fade: true,
});