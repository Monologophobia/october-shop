function handlePayment(form, billing_details) {

    if (form.classList.contains('submitting')) return false;

    form.classList.add('submitting');

    $(form).request('onStartTotalProcessing', {
        error: function(response) {
            alert(response.responseJSON.result);
        },
        success: function(data) {
            if (data) {
                if (data.result) {
                    var result = JSON.parse(data.result);
                    if (result) {

                        var form = document.createElement('form');
                        form.method = "POST";
                        form.classList.add("paymentWidgets");
                        form.dataset.brands = "VISA MASTER AMEX";
                        form.dataset.request = "onCompleteTotalProcessing";
                        form.action = result.submit_url;

                        var content = document.createElement('div');
                        content.classList.add("content");
                        content.appendChild(form);

                        var modal = document.createElement('div');
                        modal.classList.add("modal", "active");
                        modal.appendChild(content);

                        document.body.appendChild(form);

                        var script = document.createElement("script");
                        script.src = result.script_src;
                        document.body.appendChild(script);

                    }
                    console.log(result);
                }
            }
        },
        complete: function() {
            form.classList.remove('submitting');
        }
    });

}