var stripe = Stripe(stripe_key);
var elements = stripe.elements();

var style = {
    base: {
        fontSize: '14px',
        lineHeight: '24px'
    }
};

function showError(message) {
    var displayError = document.getElementById('card-errors');
    var form = displayError;
    while (true) {
        if (form.tagName == 'FORM') break;
        form = form.parentNode;
    }
    form.classList.remove('submitting');
    displayError.textContent = '';
    if (message) displayError.textContent = message;
}

var card = elements.create('card', {style: style});
card.mount('#card-element');

card.addEventListener('change', function(event) {
    showError(false);
    if (event.error) showError(event.error.message);
});

function handlePayment(form, billing_details) {

    if (form.classList.contains('submitting')) return false;

    form.classList.add('submitting');

    var method_element = createHiddenInput(form, 'payment_method_id');

    stripe.createPaymentMethod('card', card, {
        billing_details: billing_details
    }).then(function(result) {
        if (result.error) {
            showError(result.error.message);
        }
        else {
            method_element.value = result.paymentMethod.id;
            $(form).request('onComplete', {
                complete: function(response) {
                    handleOnCompleteResponse(response, form);
                }
            })
        }
    });

}

function handleOnCompleteResponse(response, form) {
    if (response.status != 200) {
        showError(response.statusText);
        return;
    }
    var data = response.responseJSON;
    if (data.error) {
        showError(data.message);
        return;
    }
    // If this isn't a straightforward success
    else if (!data.X_OCTOBER_REDIRECT) {
        handleServerResponse(form, data);
    }
}

function handleServerResponse(form, data) {

    var intent_element = createHiddenInput(form, 'payment_intent_id');

    if (data.requires_action) {
        // Use Stripe.js to handle required card action
        stripe.handleCardAction(
            data.payment_intent_client_secret
        ).then(function(result) {
            if (result.error) {
                showError(result.error.message);
            }
            else {
                intent_element.value = result.paymentIntent.id;
                $(form).request('onComplete', {
                    complete: function(response) {
                        handleOnCompleteResponse(response, form);
                    }
                });
            }
        });
    }
    else {
        showError('Payment Success. Please wait while we redirect you.');
    }
}

function createHiddenInput(form, name) {
    var element = form.querySelector('input[name="' + name + '"]');
    if (!element) {
        var e  = document.createElement('input')
        e.name = name;
        e.type = 'hidden';
        form.appendChild(e);
        element = form.querySelector('input[name="' + name + '"]');
    }
    return element;
}