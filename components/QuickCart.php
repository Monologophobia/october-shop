<?php namespace Monologophobia\Shop\Components;

use Request;
use Redirect;
use Session;

use Cms\Classes\Page;

use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Helpers\ShopHelpers;

class QuickCart extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Quick Cart',
            'description' => 'Displays the Quick Cart tool'
        ];
    }

    public function defineProperties() {
        return [
            'cartpage' => [
                'title' => 'Cart Page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getCartPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {
        $items = Session::get('cart');
        if (!$items || count($items) <= 0) {
            $this->page['cart'] = false;
        }
        else {
            $this->page['cart'] = ShopHelpers::getCartTotals();
        }
        $this->page['cart_page'] = ShopHelpers::getPageWithoutSlug($this->property('cartpage'));
    }

}
