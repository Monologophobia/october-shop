<?php namespace Monologophobia\Shop\Components;

use DB;
use Request;
use Redirect;
use Session;

use Monologophobia\Shop\Models\Product as Product;

class Search extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Search',
            'description' => 'Displays search results'
        ];
    }

    public function onRun() {

        $query = post('search');
        if (!$query) return Redirect::to('/')->with('error', 'Search terms not found');
        if (strlen($query) < 3) return Redirect::to('/')->with('error', 'Search term must be at least 3 characters');

        $results = DB::select("SELECT * FROM mono_shop_products WHERE name LIKE ? OR description LIKE ? AND deleted_at IS NULL", array('%' . $query . '%', '%' . $query . '%'));
        $products = [];
        foreach ($results as $result) {
            $product = Product::where('id', $result->id)->first();
            if ($product) {
                $product->image = $product->images[0]->getPath();
                $products[] = $product;
            }
        }

        $this->page['results'] = $products;

    }

}
