<?php namespace Monologophobia\Shop\Components;

use Request;
use Redirect;

use Cms\Classes\Page;

use Monologophobia\Shop\Models\Category as ShopCategories;
use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Helpers\ShopHelpers;

class Category extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Shop Category',
            'description' => 'Displays all products from a specific category'
        ];
    }

    public function defineProperties() {
        return [
            'slug' => [
                'title' => 'Category Slug (blank for all)',
                'type'  => 'string'
            ],
            'productpage' => [
                'title' => 'Product Page',
                'type'  => 'dropdown',
            ]
        ];
    }

    public function getProductPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {
        $this->addCss('/plugins/monologophobia/shop/assets/css/shop.css');
        $category_slug = $this->property('slug');
        if ($category_slug) {
            $category = ShopCategories::orderBy('order', 'asc')->where('slug', $category_slug)->first();
            if (!$category) {
                return Redirect::to('/')->with('error', 'Category not found');
            }

            $this->page->title = $category->name;

            $this->page['products']       = $category->products()->orderBy('order', 'asc')->get();
            $this->page['category']       = $category;
            $this->page['sub_categories'] = ShopCategories::orderBy('order', 'asc')->where('parent_id', $category->id)->with('products')->get();
            $this->page['breadcrumbs']    = ['products' => 'Products'];
            $this->page['product_page']   = ShopHelpers::getPageWithoutSlug($this->property('productpage'));

        }
        else {
            $this->page['categories'] = ShopCategories::orderBy('order', 'asc')->where('parent_id', 0)->get();
            $this->page['product_page']   = ShopHelpers::getPageWithoutSlug($this->property('productpage'));
        }

    }

}
