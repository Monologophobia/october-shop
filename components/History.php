<?php namespace Monologophobia\Shop\Components;

use Request;
use Redirect;
use Auth;

use Monologophobia\Shop\Models\Order;
use Monologophobia\Shop\Models\OrderItems;

class History extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Order History',
            'description' => 'Displays the User\'s Order History.'
        ];
    }

    public function onRun() {
        $user = Auth::getUser();
        if (!$user) {
            return Redirect::to('/login')->with('message', 'Please login');
            die();
        }

        $order_id   = intval($this->param('order_id'));
        $product_id = intval($this->param('product_id'));
        $download   = (($this->param('download') == 'download') ? true : false);

        if ($order_id && $product_id && $download) return $this->downloadProduct($user, $order_id, $product_id);

        $orders = Order::where('user_id', $user->id)->get();
        foreach ($orders as $order) {
            $order->items = OrderItems::where('order_id', $order->id)->get();
            $order->total = $order->items->sum('price');
        }
        $this->page['orders'] = $orders;
    }

    public function onDownload() {
        $product_id = intval(post('product_id'));
        $order_id   = intval(post('order_id'));
        $url        = url()->current();
        return \Redirect::to($url . '/' . $order_id . '/' . $product_id . '/download');
    }

    private function downloadProduct($user, $order_id, $product_id) {
        $order = Order::where('user_id', $user->id)->where('id', $order_id)->first();
        if (!$order) throw new \Exception("Order not found");
        $product = false;
        foreach ($order->items as $item) {
            if ($item->product_id == $product_id) {
                $product = $item->product;
                break;
            }
        }
        if (!$product) throw new \Exception ("Product not found");
        if (!$product->virtual_product) throw new \Exception ("Product is not downloadable");
        echo $product->virtual_file->output();
        die();
    }

}
