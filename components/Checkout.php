<?php namespace Monologophobia\Shop\Components;

use Auth;
use Mail;
use Request;
use Session;
use Redirect;
use Response;
use Validator;
use ValidationException;

use Cms\Classes\Page;

use Monologophobia\Shop\Models\Order;
use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Models\Voucher;
use Monologophobia\Shop\Models\Shipping;
use Monologophobia\Shop\Models\Settings;
use Monologophobia\Shop\Models\OrderItems;
use Monologophobia\Shop\Helpers\ShopHelpers;
use Monologophobia\Shop\PaymentPlugins\Payments;
use Monologophobia\Shop\PaymentPlugins\TotalProcessingPlugin as TotalProcessing;

use RainLab\User\Models\User;

class Checkout extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Checkout',
            'description' => 'Handles all Checkout responsibilities'
        ];
    }

    public function defineProperties() {
        return [
            'productpage' => [
                'title' => 'Product Page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getProductPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {

        $this->addJs('/plugins/monologophobia/shop/assets/js/checkout.js');
        
        $this->addCss('/plugins/monologophobia/shop/assets/css/shop.css');

        $items = Session::get('cart');
        $this->page['error'] = Session::pull('error');

        $this->page['checkout']  = ShopHelpers::getCartTotals();
        $this->page['user']      = Auth::getUser();
        $this->page['shipping']  = $this->getShippingOptions($this->page['checkout']->products);
        $this->page['payments']  = new Payments;
        $this->page['processor'] = Settings::get('processor', 'stripe');
        $this->page['product_page']  = ShopHelpers::getPageWithoutSlug($this->property('productpage'));

        if ($this->page["processor"] == "stripe") {
            $this->addJs("https://js.stripe.com/v3/");
            $this->addJs('/plugins/monologophobia/shop/assets/js/stripe.js');
        }

        if ($this->page["processor"] == 'total_processing') {
            $complete = isset($_GET['complete']);
            if ($complete) {
                if ($_GET['complete'] == 'true') {
                    return $this->onCompleteTotalProcessing();
                }
            }
            $this->addJs('/plugins/monologophobia/shop/assets/js/totalprocessing.js');
        }

    }

    private function getShippingOptions($products = false) {
        if (ShopHelpers::isCartSolelyVirtualProducts($products)) {
            return [
                (object) [
                    'id' => 0,
                    'name' => 'No Shipping for Virtual Products. Product will be available for download from your account after purchase',
                    'price' => 0
                ]
            ];
        }
        return Shipping::where('active', true)->get();
    }

    public function onRemoveFromCart() {
        $cart_id = post('cart_id');
        $cart    = Session::get('cart');
        foreach ($cart as $key => $value) {
            if ($value->cart_id == $cart_id) {
                unset($cart[$key]);
            }
        }
        $cart = array_values($cart);
        Session::put('cart', $cart);
        return Redirect::refresh()->with('message', 'Item Removed');
    }

    public function onUpdateUpDownQuantity() {

        $cart_id    = post('cart_id');
        $quantity   = post('quantity');
        $up_or_down = post('up_or_down');

        $cart = Session::get('cart');

        foreach ($cart as $key => $value) {
            if ($value->cart_id == $cart_id) {
                if ($up_or_down) {
                    if ($up_or_down == 'up') {
                        $cart[$key]->quantity += $quantity;
                    }
                    if ($up_or_down == 'down') {
                        $cart[$key]->quantity -= $quantity;
                        if ($cart[$key]->quantity < 1) {
                            $cart[$key]->quantity = 1;
                        }
                    }
                }
                else {
                    $cart[$key]->quantity = $quantity;
                }
            }
        }

        return Redirect::refresh()->with('message', 'Quantity Updated');

    }

    /**
     * Update cart item with an exact quantity
     * @return Redirect refresh with message
     */
    public function onUpdateExactQuantity() {
        $cart_id  = post('cart_id');
        $quantity = intval(post('quantity'));
        if ($quantity <= 0) $quantity = 1;
        $cart     = Session::get('cart');
        foreach ($cart as $key => $item) {
            if ($item->cart_id == $cart_id) {
                $cart[$key]->quantity = $quantity;
            }
        }
        return Redirect::refresh()->with('message', 'Quantity Updated');
    }

    public function onUpdateShipping() {
        return json_encode(ShopHelpers::getCartTotals());
    }

    /**
     * Take payment and complete order
     * Stripe specific
     */
    public function onComplete() {

        $data = post();
        $cart = ShopHelpers::getCartTotals();
        $shipping = $this->getShipping($data);
        if (!$data['billing'] || !$shipping) throw new \AjaxException('Some Information was missing');

        $guest_checkout = (isset($data['guest_checkout']) ? boolval($data['guest_checkout']) : false);
        $user = Auth::getUser() ? Auth::getUser() : $this->getUser($data['billing'], $guest_checkout);

        $payments = new Payments;
        $payment  = $payments->process($user, $cart, (object) $data);
        if (in_array(get_class($payment), ["Illuminate\Http\JsonResponse", "Response"])) return $payment;

        if (!$guest_checkout) {
            $user->save();
            \Auth::login($user);
        }

        return $this->completeOrder($user, $data, $shipping, $cart, $guest_checkout, $payment->payment_id);

    }

    /**
     * Start take payment via Total Processing
     */
    public function onStartTotalProcessing() {
        try {

            // gather data
            $data = post();
            $shipping = $this->getShipping($data);
            $cart     = ShopHelpers::getCartTotals();
            if (!$data['billing'] || !$shipping) throw new \Exception('Some Information was missing');
            $guest_checkout = (isset($data['guest_checkout']) ? boolval($data['guest_checkout']) : false);
            $user = Auth::getUser() ? Auth::getUser() : $this->getUser($data['billing'], $guest_checkout);

            $payments = new TotalProcessing;
            $checkout = $payments->prepareCheckout($cart->total);

            // store data in a session variable as we can't post from the card details screen
            // can't save user as it loses model or cart for same reason
            // voucher discount is also passed as it's lost in the post data
            $array = [
                'data' => $data,
                'shipping' => $shipping,
                'guest_checkout' => $guest_checkout,
                'discount_amount' => $cart->discount_amount ?? 0,
                'shipping_price'  => $cart->shipping ?? 0,
            ];

            \Session::put('checkout_data', json_encode($array));

            return json_encode($checkout);

        }
        catch (\Throwable $e) {
            \Log::error($e);
            throw new \AjaxException($e->getMessage());
        }
    }

    /**
     * Take money and complete order via Total Processing
     */
    public function onCompleteTotalProcessing() {
        try {

            $payments   = new TotalProcessing;
            $payment_id = $_GET['id'];
            $status_url = $_GET['resourcePath'];
            $status     = $payments->getPaymentStatus($status_url);

            // if payment was a success
            if (
            // https://totalprocessing.docs.oppwa.com/reference/resultCodes#
            preg_match("/^(000\.000\.|000\.100\.1|000\.[36])/", $status->result->code) ||
            preg_match("/^(000\.400\.0[^3]|000\.400\.100)/", $status->result->code) ||
            preg_match("/^(000\.200)/", $status->result->code)) {

                $array = \Session::pull('checkout_data');
                $array = json_decode($array, true);

                $data     = $array['data'];
                $shipping = $array['shipping'];

                $cart     = ShopHelpers::getCartTotals();
                $cart->discount_amount = $array['discount_amount'];
                $cart->shipping        = $array['shipping_price'];

                $guest_checkout = $array['guest_checkout'];
                $user = Auth::getUser() ? Auth::getUser() : $this->getUser($data['billing'], $guest_checkout);

                if (!$guest_checkout) {
                    $user->save();
                    \Auth::login($user);
                }

                return $this->completeOrder($user, $data, $shipping, $cart, $guest_checkout, $payment_id);

            }
            else {
                throw new \Exception($status->result->description);
            }

        }
        catch (\Throwable $e) {
            \Log::error($e);
            return \Redirect::to(url()->current())->with('error', $e->getMessage());
        }
    }

    /**
     * Once payment has been taken, complete the order and insert all relevant records
     * @param User
     * @param Array of post'd data
     * @param Array of shipping data
     * @param Object Cart
     * @param Boolean Guest Checkout
     * @param String Payment Intent ID
     */
    public function completeOrder($user, $data, $shipping, $cart, $guest_checkout, $payment_id) {

        // Insert an order and all the lines
        $order = $this->insertOrder($user, $data['billing'], $shipping, $cart->shipping, $data['postage'], $data['voucher'], $cart->discount_amount, $guest_checkout);
        if ($order) {
            foreach ($cart->products as $product) {
                $this->insertOrderItem($order->id, $product);
            }
        }

        Session::forget('cart');

        $params = [
            'name'     => $user->name,
            'date'     => date('Y-m-d'),
            'bill_to'  => json_decode($order->bill_to),
            'ship_to'  => json_decode($order->ship_to),
            'products' => $this->generateProductsForEmail($cart->products),
            'shipping' => $cart->shipping
        ];
        $email = $user->email;
        $name  = $user->name . ' ' . $user->surname;
        try {
            Mail::send('monologophobia.shop::shop.orders', $params, function($message) use ($email, $name) {
                $message->to($email, $name);
            });
        }
        catch (\Throwable $e) {}

        $this->sendNotificationEmails($cart, $user, $order);

        // remove quantity from product
        try {
            foreach ($cart->products as $product) {
                $product->decrement('stock_quantity', $product->quantity);
            }
        }
        catch (\Exception $e) {
            \Log::error($e);
        }

        Session::put('order_complete', $order->id);

        // try and do the payment transfer system to third party sellers. don't worry too much so just log a failure and move on
        try {
            $processor = Settings::get('processor', 'stripe');
            // stripe only
            if ($processor == "stripe") {
                if ($this->cartHasDifferentPayees($cart)) {
                    $this->handlePaymentTransfers($cart, $payment_id);
                }
            }
        }
        catch (\Exception $e) {
            \Log::error($e);
        }

        return Redirect::to('/')->with('message', 'Thanks! Your Order is complete.');

    }

    /**
     * Generates shipping data from supplied data
     * @param POST'd data
     * @return Array of shipping info
     */
    private function getShipping($data) {
        $billing = $data['billing'];
        if (boolval($data['_shipping_same_as_billing'])) {
            $shipping = [];
            foreach ($billing as $key => $value) {
                $shipping[$key] = $value;
            }
            return $shipping;
        }
        else {
            return $data['shipping'];
        }
    }

    /**
     * Checks through the cart products to see if any have
     * a specific seller_stripe_id. If they do, this means
     * that a percentage of the sale has to go to a different
     * seller, not just ourselves
     * @param ShopHelpers::getCartTotals();
     * @return boolean
     */
    public static function cartHasDifferentPayees($cart) {

        foreach ($cart->products as $product) {
            if ($product->seller) {
                if ($product->seller->stripe_id) return true;
            }
        }

        return false;

    }

    private function handlePaymentTransfers($cart, $payment_id) {

        // create an array of unique sellers
        $sellers = [];
        foreach ($cart->products as $product) {
            if ($product->seller) {
                if ($product->seller->stripe_id && $product->seller_percentage) {
                    // if we don't recognise this seller in the array, create it
                    if (!isset($sellers[$product->seller->stripe_id])) {
                        $sellers[$product->seller->stripe_id] = 0;
                    }
                    // calculate percentage to transfer to seller
                    $amount = ($product->total_price / 100) * $product->seller_percentage;
                    // round to 2
                    $sellers[$product->seller->stripe_id] = round($amount, 2);
                }
            }
        }

        // Stripe Transfer doesn't accept payment intent ID as source_transaction
        // therefore, find the intent and get the charge information from it
        $payments = new Payments;
        $charge   = $payments->getChargeFromPaymentIntent($payment_id);

        // process the transfers
        foreach ($sellers as $stripe_id => $amount) {
            try {
                \Stripe\Transfer::create(array(
                    "amount"             => $amount * 100,
                    "currency"           => "gbp",
                    "source_transaction" => $charge ? $charge->id : null,
                    "destination"        => $stripe_id,
                ));
            }
            catch (\Exception $e) {
                \Log::error($e);
            }
        }

    }

    private function getUser($billing, bool $guest_checkout) {

        $user = User::withTrashed()->where('email', $billing['email'])->first();

        if (!$user) {
            $user = new User;
        }
        else {
            $user->deleted_at = null;
        }

        $user->name     = $billing['name'];
        $user->surname  = $billing['surname'];
        $user->email    = $billing['email'];
        $user->username = $user->email;

        // if not guest checkout
        if (!$guest_checkout) {

            $user->password  = $billing['password'];
            $user->password_confirmation = $billing['password_confirmation'];

            $user->is_activated = 1;

            // toArray drops password fields so validation fails. Re-add them
            $user_array = $user->toArray();
            $user_array['password'] = $billing['password'];
            $user_array['password_confirmation'] = $billing['password_confirmation'];

            $validation = Validator::make($user_array, $user->rules);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

        }

        return $user;

    }

    private function insertOrder($user, $billing, $shipping, $shipping_price, $shipping_id, $voucher, $discount, bool $guest_checkout) {

        $voucher = Voucher::where('code', $voucher)->first();

        // sanitise user inputs
        $billing  = $this->sanitiseBillingShippingData($billing);
        $shipping = $this->sanitiseBillingShippingData($shipping);

        $order = new Order;
        if (!$guest_checkout) $order->user_id = $user->id;
        $order->shipping_id    = $shipping_id;
        $order->bill_to        = json_encode($billing);
        $order->ship_to        = json_encode($shipping);
        $order->shipping_price = floatval($shipping_price);
        $order->voucher_used   = ($voucher ? $voucher->code : false);
        $order->discount       = $discount;
        $order->save();
        return $order;

    }

    /**
     * Sanitise billing or shipping data as it may come directly from POST
     * @param Array
     * @return Array sanitised
     */
    private function sanitiseBillingShippingData(array $data) {
        return [
            "name"     => filter_var($data["name"], FILTER_SANITIZE_STRING),
            "surname"  => filter_var($data["surname"], FILTER_SANITIZE_STRING),
            "email"    => filter_var($data["email"], FILTER_SANITIZE_EMAIL),
            "address1" => filter_var($data["address1"], FILTER_SANITIZE_STRING),
            "address2" => filter_var($data["address2"], FILTER_SANITIZE_STRING),
            "town"     => filter_var($data["town"], FILTER_SANITIZE_STRING),
            "country"  => filter_var($data["country"], FILTER_SANITIZE_STRING),
            "postcode" => filter_var($data["postcode"], FILTER_SANITIZE_STRING),
        ];
    }

    private function insertOrderItem($order_id, $product) {
        $order_item = new OrderItems;
        $order_item->order_id   = $order_id;
        $order_item->product_id = $product->id;
        $order_item->quantity   = $product->quantity;
        $order_item->custom     = $product->custom;
        $order_item->price      = floatval($product->price);
        $order_item->shipped    = false;
        if ($product->discount_price) {
            $order_item->price  = floatval($product->discount_price);
        }
        $order_item->save();
    }

    private function generateProductsForEmail($products) {

        $email_products = [];

        foreach ($products as $product) {

            $return = new \stdclass;
            $return->name     = $product->name;
            $return->quantity = $product->quantity;
            $return->custom   = [];
            $return->virtual_product = false;

            // get totals
            $total = floatval($product->price) * intval($product->quantity);
            if ($product->discount_price) $total = floatval($product->discount_price) * intval($product->quantity);
            $return->total = $total;

            if ($product->virtual_product) {
                $return->virtual_product = true;
            }

            if ($product->custom) {
                foreach ($product->custom as $key => $value) {
                    $return->custom[$key] = $value;
                }
            }

            $email_products[] = $return;

        }

        return $email_products;

    }

    private function sendNotificationEmails($cart, $user, $order) {
        try {

            $this->sendSellerNotifications($cart, $user, $order);

            if ($notification_emails = Settings::get('sale_notification_email')) {

                $notification_emails = explode(",", $notification_emails);
                $params = [
                    'ship_to'  => json_decode($order->ship_to),
                    'products' => $this->generateProductsForEmail($cart->products)
                ];

                foreach ($notification_emails as $notification) {
                    Mail::send('monologophobia.shop::shop.notification', $params, function($message) use ($notification) {
                        $message->to($notification);
                    });
                }

            }

        }
        catch (\Exception $e) {
            \Log::error($e);
        }
    }

    /**
     * Look through the cart products to see if any have a notification email
     * If they do, we want to send an email to that seller
     * go through all the products, identify unique sellers and generate an email
     * to send to them
     * @param getCartTotals
     * @param User model
     */
    private function sendSellerNotifications($cart, $user, $order) {

        $sellers = [];
        foreach ($cart->products as $product) {
            try {
                if ($product->seller) {
                    if ($product->seller->notify) {
                        if (!isset($sellers[$product->seller->email])) {
                            $sellers[$product->seller->email] = [];
                        }
                        $sellers[$product->seller->email][] = $product;
                    }
                }
            }
            catch (\Exception $e) {
                \Log::error($e);
            }
        }

        foreach ($sellers as $email => $products) {

            try {

                $params = [
                    'ship_to'  => json_decode($order->ship_to),
                    'products' => $this->generateProductsForEmail($products)
                ];

                Mail::send('monologophobia.shop::shop.notification', $params, function($message) use ($email) {
                    $message->to($email);
                });

            }
            catch (\Exception $e) {
                \Log::error($e);
            }

        }

    }

}

