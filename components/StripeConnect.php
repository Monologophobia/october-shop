<?php namespace Monologophobia\Shop\Components;

use Log;
use Response;
use Redirect;
use Exception;

use \GuzzleHttp\Client;
use Illuminate\Routing\Controller;

use Monologophobia\Shop\Models\Seller;
use Monologophobia\Shop\Models\Settings;

class StripeConnect extends Controller {

    public function connectedSeller() {
        try {

            // get response scope, code, and state
            $scope = get('scope');
            $code  = get('code');
            $state = get('state');

            // if there are any errors, throw them
            $error = get('error');
            if ($error) throw new Exception(get('error_description'));

            // if no details have been supplied, display 404
            if (!$scope || !$code || !$state) {
                $this->setStatusCode(404);
                return $this->controller->run('404');
            }

            $seller = Seller::where('state', $state)->firstOrFail();

            // use the supplied code to do an authorisation event to get the
            // stripe user's refresh_token, access_token, and stripe_user_id
            $live = boolval(Settings::get('live'));
            $key  = ($live ? Settings::get('app_secret') : Settings::get('test_secret'));
            $client = new Client();
            $response = $client->post('https://connect.stripe.com/oauth/token', [
                'form_params' => [
                    'client_secret' => $key,
                    'code'          => $code,
                    'grant_type'    => 'authorization_code'
                ]
            ]);
            $stripe_user = json_decode($response->getBody());

            if (isset($stripe_user->error)) throw new Exception($stripe_user->error_description);

            // store the returned details
            $seller->stripe_id     = $stripe_user->stripe_user_id;
            $seller->refresh_token = $stripe_user->refresh_token;
            $seller->access_token  = $stripe_user->access_token;
            $seller->save();

            return Redirect::to('/')->with('message', 'Your Stripe account has been succesfully linked.');

        }
        catch (Exception $e) {
            Log::error($e);
            return Redirect::to('/')->with('error', $e->getMessage());
        }
    }

    public function handleWebhook() {

        try {

            // init stripe
            $live = boolval(Settings::get('live'));
            $key  = ($live ? Settings::get('app_secret') : Settings::get('test_secret'));
            $webhook_signing_key = ($live ? Settings::get('webhook_secret') : Settings::get('test_webhook_secret'));
            \Stripe\Stripe::setApiKey($key);

            $payload = @file_get_contents("php://input");
            $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $webhook_signing_key
            );

            if (isset($event)) {

                $failure_states = [
                    "account.application.deauthorized",
                    "account.external_account.deleted",
                ];

                if (in_array($event->type, $failure_states)) {
                    $seller = Seller::where('stripe_id', $event->data->object->stripe_user_id)->firstOrFail();
                    $seller->stripe_id = null;
                    $seller->save();
                }

            }

            // report everything's fine
            http_response_code(200);

        }

        // Invalid payload
        catch (\UnexpectedValueException $e) {
            http_response_code(400);
        }

        // Invalid signature
        catch (\Stripe\Error\SignatureVerification $e) {
            http_response_code(400);
        }

        // something else went wrong. Log it and send a 500 error
        catch (Exception $e) {
            Log::error($e);
            http_response_code(500);
        }

        die();

    }

}

?>
