<?php namespace Monologophobia\Shop\Components;

use Request;
use Redirect;
use Session;
use Validator;

use Cms\Classes\Page;

use Monologophobia\Shop\Models\Product;
use Monologophobia\Shop\Helpers\ShopHelpers;
use Monologophobia\Shop\Models\ProductReview;

class ProductPage extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Product Page',
            'description' => 'Displays the Product Page'
        ];
    }

    public function defineProperties() {
        return [
            'id' => [
                 'title' => 'Product ID to display',
                 'type'  => 'string'
            ],
            'cartpage' => [
                'title' => 'Cart Page',
                'type'  => 'dropdown',
            ]
        ];
    }

    public function getCartPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function onRun() {

        $id = $this->property('id');

        $product = Product::where('slug', $id)->orWhere('id', $id)->first();
        if (!$product) return Redirect::to('/')->with('error', 'Product Not Found');

        $this->page->title = $product->name;
        $this->page['product'] = $product;
        $this->page['related'] = Product::where('category_id', $product->category_id)->take(5)->get();

        $added_to_cart = Session::pull('added_to_cart', false);
        $this->page['added_to_cart'] = ($added_to_cart ? Product::find($added_to_cart) : false);

        $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css');
        $this->addJs('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js');
        $this->addCss("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css");
        $this->addCss("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css");
        $this->addJs('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js');
        $this->addCss('/plugins/monologophobia/shop/assets/css/shop.css');
        $this->addJs('/plugins/monologophobia/shop/assets/js/product.js');

        $this->page['cart_page']  = ShopHelpers::getPageWithoutSlug($this->property('cartpage'));
        $this->page['cart_total'] = ShopHelpers::getCartTotals();

        $this->page['message'] = \Session::pull('message', false);

    }

    public function onAddToCart() {
        $id = post('id');
        $quantity = post('quantity');
        $custom   = post('custom');
        if (!$id || !$quantity) return Redirect::to('/')->with('error', 'Something went wrong. Please try again');
        if (ShopHelpers::addItemToCart(intval($id), intval($quantity), $custom)) {
            Session::put('added_to_cart', intval($id));
            return Redirect::back()->with('message', 'Item added to Cart');
        }
        else {
            return Redirect::back()->with('error', 'Product not Found');
        }
    }

    public function onSubmitReview() {

        $email = filter_var(post('email'), FILTER_SANITIZE_EMAIL);
        if ($email) return Redirect::refresh()->with('message', 'Thanks');

        $product = Product::find(intval(post('product_id')));
        if (!$product) return Redirect::refresh()->with('message', 'Product not found');

        $review = new ProductReview;
        $review->name       = filter_var(post('name'), FILTER_SANITIZE_STRING);
        $review->email      = filter_var(post('emailaddress'), FILTER_SANITIZE_EMAIL);
        $review->rating     = intval(post('rating'));
        $review->comments   = filter_var(post('comments'), FILTER_SANITIZE_STRING);
        $review->headline   = filter_var(post('headline'), FILTER_SANITIZE_STRING);
        $review->location   = filter_var(post('location'), FILTER_SANITIZE_STRING);
        $review->product_id = $product->id;
        $review->published  = false;

        $validation = Validator::make($review->toArray(), $review->rules);
        if ($validation->fails()) {
            throw new \AjaxException($validation->messages()->first());
        }

        $review->save();

        return Redirect::refresh()->with('message', 'Thanks for your review. It is awaiting moderation.');

    }

}
