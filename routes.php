<?php

// Static route for Stripe Webhooks
// point your webhooks to https://your-url.tld/stripe/webhooks
Route::middleware(['web'])->get('api/shop/stripe-connect', 'Monologophobia\Shop\Components\StripeConnect@connectedSeller');
Route::post('api/shop/stripe-connect', 'Monologophobia\Shop\Components\StripeConnect@handleWebhook');